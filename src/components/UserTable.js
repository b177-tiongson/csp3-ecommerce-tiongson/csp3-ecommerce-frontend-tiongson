import { useState, useEffect } from 'react';
//import './styles/table.css'
// import PropTypes from 'prop-types';
// import { Card, Button, Container } from 'react-bootstrap';
import React from 'react'
import Table from 'react-bootstrap/Table'



export default function UserTable({data,column}){


    const TableHeadItem = ({item}) => <th>{item.heading}</th>
    const TableRow =({item,data}) =>(
        
        <tr>
            <td>{item.firstName} {item.lastName}</td>
            <td>{item.email}</td>
            <td>{item.address} </td>
            <td>{item.mobileNo}</td>
            {item.isAdmin?
                <td>Admin</td>
                :
                <td>Non Admin</td>
            }
            
        </tr>

    )


	return (

        <Table responsive>
            <thead>
                <tr>
                    {column.map((item,index)=><TableHeadItem item={item}/>)}
                </tr>
            </thead>
            <tbody>
                {data.map((item,index)=><TableRow item={item} column={column}/>)}
            </tbody>
        </Table>

	)


}