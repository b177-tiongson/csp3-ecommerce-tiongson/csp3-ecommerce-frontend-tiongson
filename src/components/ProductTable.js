import { useState, useEffect } from 'react';
//import './styles/table.css'
// import PropTypes from 'prop-types';
import { Button, Container } from 'react-bootstrap';
import React from 'react'
import Table from 'react-bootstrap/Table'
import { useNavigate } from 'react-router-dom'



export default function ProductTable({data,column}){


    const history = useNavigate()


    const TableHeadItem = ({item}) => <th>{item.heading}</th>
    const TableRow =({item,data}) =>(
        
        <tr>
            <td>{item.productName}</td>
            <td>{item.productDescription}</td>
            <td>{item.category} </td>
            <td>{item.price}</td>
            <td>{item.stock}</td>
            <td>{item.createdOn}</td>
            {item.isActive?
                <td>Available</td>
                :
                <td>Archived/Out of Stock</td>
            }
            
        </tr>

    )


	return (

        <Container>

        <Table responsive>
            <thead>
                <tr>
                    {column.map((item,index)=><TableHeadItem item={item}/>)}
                </tr>
            </thead>
            <tbody>
                {data.map((item,index)=><TableRow item={item} column={column}/>)}
            </tbody>
        </Table>

        <Button onClick={e=>{
            history("/addProduct")
        }}>Add Product</Button>

        </Container>

	)


}