import React, { Fragment, useEffect, useState, useContext } from 'react';
import { Button, Row, Col, Tab, Tabs } from 'react-bootstrap';
import { Link } from 'react-router-dom';
//import Table from 'react-bootstrap/Table'
import ViewUsers from './ViewUsers'
import UserContext from '../UserContext';

export default function HomeAdmin(){

	const [key, setKey] = useState('admin');

	const { user } = useContext(UserContext);

	const [userAll, setUserAll] = useState([]);

	const fetchUser = () => {
		fetch('http://localhost:4000/users/all')
		.then(res => res.json())
		.then(data => {
			console.log(data);

			//Sets the "courses" state to map the data retrieved from the fetch request into several "CourseCard" component
			setUserAll(data.map(user => {
				
				return (
					<ViewUsers key={userAll._id} courseProp={userAll} />
				);
			}))
		})
	}

	useEffect(() => {
		fetchUser()
	}, []);

	return (

		<Row>

			<Tabs
			      id="controlled-tab-example"
			      activeKey={key}
			      onSelect={(k) => setKey(k)}
			      className="my-5"
			    >
			      <Tab eventKey="home" title="Products">
			        {userAll}
			      </Tab>
			      <Tab eventKey="profile" title="Users">
			      <ViewUsers />
			       
			      </Tab>

			    </Tabs>


		</Row>
	)
}