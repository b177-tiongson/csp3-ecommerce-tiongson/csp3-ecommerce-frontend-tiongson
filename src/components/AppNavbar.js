import React, { useState, useContext } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavbar(){
	// State to store the user information stored in the login page
	// const [user, setUser] = useState(localStorage.getItem("email"));
	// console.log(user);

	const { user } = useContext(UserContext);

	return (

		(user.id==null)?
		<Navbar className="navColor" expand="lg">
			<Navbar.Brand className="d-flex" as={Link} to="/">RDNT</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
			<Nav className="me-auto">
			  <Nav.Link as={Link} to="/" exact="true">Home</Nav.Link>
			</Nav>
			<Nav>
				<React.Fragment >
					<Nav.Link as={Link} to="/register" exact="true">Register</Nav.Link>
					<Nav.Link as={Link} to="/login" exact="true">Login</Nav.Link>
				</React.Fragment>
			</Nav>

			</Navbar.Collapse>

		</Navbar>
		:

		(user.isAdmin==true)?

		<Navbar className="navColor" expand="lg">
			<Navbar.Brand className="d-flex" as={Link} to="/admin">RDNT</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
			<Nav className="me-auto">
			  <Nav.Link as={Link} to="/admin" exact="true">Dashboard</Nav.Link>
			</Nav>
			<Nav>
				<React.Fragment >
					<Nav.Link as={Link} to="/logout" exact="true">Logout</Nav.Link>
				</React.Fragment>
			</Nav>

			</Navbar.Collapse>

		</Navbar>


		:

		<Navbar className="navColor" expand="lg">
			<Navbar.Brand className="d-flex" as={Link} to="/">RDNT</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
			<Nav className="me-auto">
			  <Nav.Link as={Link} to="/" exact="true">Home</Nav.Link>
			</Nav>
			<Nav>
				<React.Fragment >
					<Nav.Link as={Link} to="/logout" exact="true">Logout</Nav.Link>
				</React.Fragment>
			</Nav>

			</Navbar.Collapse>

		</Navbar>








		
	)
}


{/*<Navbar className="navColor" expand="lg">
    <Navbar.Brand className="d-flex" as={Link} to="/">RDNT</Navbar.Brand>
    <Navbar.Toggle aria-controls="basic-navbar-nav" />
    <Navbar.Collapse id="basic-navbar-nav">
      <Nav className="me-auto">
        <Nav.Link as={Link} to="/admin" exact="true">Home</Nav.Link>
        <Nav.Link as={Link} to="/addProduct" exact="true">Add Product</Nav.Link>
      </Nav>


      <Nav>
      	 {(user.id !== null) ?

      	 	<Nav.Link as={Link} to="/logout" exact="true">Logout</Nav.Link>
      	 	:
      	
      	 	<React.Fragment >
      	 		<Nav.Link as={Link} to="/register" exact="true">Register</Nav.Link>
      	 		<Nav.Link as={Link} to="/login" exact="true">Login</Nav.Link>
      	 	</React.Fragment>
      	 }
        
      </Nav>

     	
       
        
        
      
    </Navbar.Collapse>
  
</Navbar>*/}

