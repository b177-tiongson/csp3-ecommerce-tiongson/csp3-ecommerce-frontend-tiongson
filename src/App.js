import { Fragment, useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom'
import AppNavbar from './components/AppNavbar';
//import CourseView from './components/CourseView'
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';
//import Courses from './pages/Courses';
//import Error from './pages/Error';
import AdminDashboard from './pages/AdminDashboard';
import Home from './pages/Home';
import Logout from './pages/Logout';
import Register from './pages/Register';
import Login from './pages/Login';
import AddProduct from './pages/AddProduct';
import './App.css';
import { UserProvider } from './UserContext';

function App() {
  // State hook for the user state that's defined for a global scope
  const [user, setUser] = useState({
    //email: localStorage.getItem('email')
    id: null,
    isAdmin: null
  })

  // Function for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear();
  }

  //used to check if the user information is properly stored during login and localStorage information is cleared upon logout
/*  useEffect(()=>{
    console.log(user)
    console.log(localStorage)
  },[user])*/


  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
          <AppNavbar />
          <Container>
            <Routes>

            <Route path="/" element={<Home />} />
            <Route path="/admin" element={<AdminDashboard />} />
            <Route path="/register" element={<Register />} />
            <Route path="/login" element={<Login />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="/addProduct" element={<AddProduct />} />

            {/*
            
            <Route path="*" element={<Error />} />*/}

            </Routes>
          </Container>
      </Router>
    </UserProvider>




    
  );
}

export default App;



