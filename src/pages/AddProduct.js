import {useState, useEffect, useContext} from 'react'
import {Form, Button} from 'react-bootstrap'
import UserContext from '../UserContext';
import { Navigate, Link, useNavigate } from 'react-router-dom'
import Swal from 'sweetalert2'
//import FileBase64 from 'react-file-base64'

export default function AddProduct(){

	const { user, setUser } = useContext(UserContext)

	const history = useNavigate()

	const [productName, setProductName]=useState('')
	const [productDescription, setProductDescription]=useState('')
	const [price, setPrice]=useState('')
	const [stock, setStock]=useState('')
	const [category, setCategory]=useState('')
	const [image, setImage]=useState({})


	function addProduct(e){
		e.preventDefault()

	
		
		console.log(image)

		fetch('http://localhost:4000/products/addProduct',{
			method:'POST',
			headers:{'Content-Type': 'application/json'},
			body: JSON.stringify({
				productName: productName,
				productDescription: productDescription,
				price: price,
				stock: stock,
				category: category,
				image: image
			})
		})
		.then(res=>res.json())
		.then(data=>{

			if(data===true){

				Swal.fire({
				    title: `Product ${productName} Added!`,
				    icon: "success"
				})
				//history("/login")

			}
			else{

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})

			}


		})

	}

	return(

		<Form onSubmit={(e)=> addProduct(e)}>
			<h1>Add Product</h1>

			<Form.Group className="mb-3" controlId="productName">
			  <Form.Label>Product Name</Form.Label>
			  <Form.Control 
			  	type="text" 
			  	placeholder="Enter Product Name" 
			  	value={productName}
			  	onChange={e=> setProductName(e.target.value)}
			  	required
			  />
			</Form.Group>

			<Form.Group className="mb-3" controlId="productDescription">
			  <Form.Label>Description</Form.Label>
			  <Form.Control 
			  	type="text" 
			  	placeholder="Enter Product Description" 
			  	value={productDescription}
			  	onChange={e=> setProductDescription(e.target.value)}
			  	required
			  />
			</Form.Group>

			<Form.Group className="mb-3" controlId="price">
			  <Form.Label>Price</Form.Label>
			  <Form.Control 
			  	type="number" 
			  	placeholder="Enter Price" 
			  	value={price}
			  	onChange={e=> setPrice(e.target.value)}
			  	required
			  />
			</Form.Group>

			<Form.Group className="mb-3" controlId="stock">
			  <Form.Label>Stock</Form.Label>
			  <Form.Control 
			  	type="number" 
			  	placeholder="Enter Stock Qty." 
			  	value={stock}
			  	onChange={e=> setStock(e.target.value)}
			  	required
			  />
			</Form.Group>

			<Form.Group className="mb-3" controlId="category">
			  <Form.Label>Category</Form.Label>

			  	{/*<DropdownButton variant="outline-secondary" title="Dropdown" id="input-group-dropdown-1">
			  		<Dropdown.Item href="#">Action</Dropdown.Item>
			    	<Dropdown.Item href="#">Another action</Dropdown.Item>
			    	<Dropdown.Item href="#">Something else here</Dropdown.Item>
				</DropdownButton>*/}  

			  <Form.Control 
			  	type="text" 
			  	placeholder="Enter Category" 
			  	value={category}
			  	onChange={e=> setCategory(e.target.value)}
			  	required
			  />
			</Form.Group>

			<div>
			<input 
				type="file"
				//value={image}
				onChange={(e)=>{

					let x = e.target.files[0].lastModified.toString()
					
					setImage(x+".jpeg")
					//console.log(e.target.files[0])
				}}
				

			/>
			</div>

		
			{/*<Button className="my-3" variant="primary" type="file" id="submitBtn"> Upload Image</Button>*/}

			<Button className="my-3" variant="primary" type="submit" id="submitBtn"> Submit</Button>


		  	
		</Form>



	)


}
