import { useEffect, useState, useContext } from 'react';
import UserTable from '../components/UserTable';
import ProductTable from '../components/ProductTable';
import UserContext from '../UserContext'
import Tab from 'react-bootstrap/Tab';
import Tabs from 'react-bootstrap/Tabs';


export default function AdminDashboard(){



	const { user, setUser } = useContext(UserContext)

	const [userTable, setUserTable] = useState([])
	const [productTable, setProductTable] = useState([])

	const column = [

		{heading:'Name', value:'name'},
		{heading:'Email' , value:'email'},
		{heading:'Address' , value:'address'},
		{heading:'Mobile Number' , value:'mobileNo'},
		{heading:'isAdmin' , value:'isAdmin'}

	]

	const column2 = [

		{heading:'Product Name', value:'name'},
		{heading:'Description' , value:'description'},
		{heading:'Category' , value:'category'},
		{heading:'Price' , value:'price'},
		{heading:'Stock' , value:'stock'},
		{heading:'Date Created' , value:'dateCreate'},
		{heading:'isAvailable' , value:'isAvailable'}

	]

	useEffect(() => {

		fetch('http://localhost:4000/users/all')
		.then(res => res.json())
		.then(data => setUserTable(data))
		.catch(err=>console.log(err))

		fetch('http://localhost:4000/products/all')
		.then(res => res.json())
		.then(data => setProductTable(data))
		.catch(err=>console.log(err))



	}, []);

	return(

		<Tabs defaultActiveKey="users" id="uncontrolled-tab-example" className="my-3" >
			<Tab eventKey="users" title="User">
			  <UserTable data={userTable} column={column}/>
			</Tab>

			<Tab eventKey="products" title="Products">
				<ProductTable data={productTable} column={column2}/>

			</Tab>

		</Tabs>



		
	)

}


