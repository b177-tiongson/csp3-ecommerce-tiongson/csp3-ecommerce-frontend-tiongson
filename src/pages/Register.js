import {useState, useEffect, useContext} from 'react'
import {Form, Button} from 'react-bootstrap'
import UserContext from '../UserContext';
import { Navigate, Link, useNavigate } from 'react-router-dom'
import Swal from 'sweetalert2'

export default function Register(){

	const { user, setUser } = useContext(UserContext)

	const history = useNavigate()

	const [firstName, setFirstName]=useState('')
	const [lastName, setLastName]=useState('')
	const [mobileNumber, setMobileNumber]=useState('')
	const [email, setEmail]=useState('')
	const [address, setAddress]=useState('')
	const [password1, setPassword1]=useState('')
	const [password2, setPassword2]=useState('')


	const [isActive, setIsActive]= useState(false)

	function registerUser(e){
		e.preventDefault()

		fetch('http://localhost:4000/users/checkEmail',{

			method:'POST',
			headers:{
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email:email
			})

		})
		.then(res=>res.json())
		.then(data=>{

			console.log("first fetch:" + data)


			if(data===false){

				return fetch('http://localhost:4000/users/register',{
					method:'POST',
					headers:{
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						mobileNo: mobileNumber,
						address: address,
						email: email,
						password: password1
					})

				})
				.then(res=>res.json())
				.then(data=>{
					console.log("second fetch:" + data)

					if(data===true){

						Swal.fire({
						    title: "Registration Successful",
						    icon: "success",
						    text: "Welcome to Zuitt. You may now login."
						})
						history("/login")

					}
					else{

						Swal.fire({
							title: "Something went wrong",
							icon: "error",
							text: "Please try again."
						})

					}
				})

			}
			else{

				Swal.fire({
					title: "Duplicate email found",
					icon: "error",
					text: "Please Provide a different email."
				})

			}

		})
	}

	useEffect(()=>{
		//validation to enable submit button when all fields are populated and both password match
		if((email!=='' && password1!=='' && password2!=='' && firstName!=='' && lastName!=='' && mobileNumber!==''&& address!=='') && (password1 === password2) && (mobileNumber.length ==11) ){

			setIsActive(true)
		}
		else{
			setIsActive(false)
		}

		//if(mobileNumber)
	},[email,password1,password2,firstName,lastName,mobileNumber,address])

	return(

		(user.id !== null) ?
		    <Navigate to="/courses" />
		:

		<Form onSubmit={(e)=> registerUser(e)}>
			<h1>Register</h1>

			<Form.Group className="mb-3" controlId="userFirstName">
			  <Form.Label>First Name</Form.Label>
			  <Form.Control 
			  	type="text" 
			  	placeholder="Enter First Name" 
			  	value={firstName}
			  	onChange={e=> setFirstName(e.target.value)}
			  	required
			  />
			</Form.Group>

			<Form.Group className="mb-3" controlId="userLastName">
			  <Form.Label>Last Name</Form.Label>
			  <Form.Control 
			  	type="text" 
			  	placeholder="Enter Last Name" 
			  	value={lastName}
			  	onChange={e=> setLastName(e.target.value)}
			  	required
			  />
			</Form.Group>

			<Form.Group className="mb-3" controlId="userEmail">
			  <Form.Label>Email address</Form.Label>
			  <Form.Control 
			  	type="email" 
			  	placeholder="Enter Email" 
			  	value={email}
			  	onChange={e=> setEmail(e.target.value)}
			  	required
			  />
			</Form.Group>

			<Form.Group className="mb-3" controlId="userMobileNumber">
			  <Form.Label>Mobile Number</Form.Label>
			  <Form.Control 
			  	type="text" 
			  	placeholder="Enter Mobile Number" 
			  	value={mobileNumber}
			  	onChange={e=> setMobileNumber(e.target.value)}
			  	required
			  />
			</Form.Group>

			<Form.Group className="mb-3" controlId="userAddress">
			  <Form.Label>Address</Form.Label>
			  <Form.Control 
			  	type="text" 
			  	placeholder="Enter Address" 
			  	value={address}
			  	onChange={e=> setAddress(e.target.value)}
			  	required
			  />
			</Form.Group>

			<Form.Group className="mb-3" controlId="password1">
			  <Form.Label>Password</Form.Label>
			  <Form.Control 
			  	type="password" 
			  	placeholder="Password"
			  	value={password1}
			  	onChange={e=> setPassword1(e.target.value)} 
			  	required
			  />
			</Form.Group>

			<Form.Group className="mb-3" controlId="password2">
			  <Form.Label>Verify Password</Form.Label>
			  <Form.Control 
			  	type="password" 
			  	placeholder="Verify Password"
			  	value={password2}
			  	onChange={e=> setPassword2(e.target.value)}  
			  	required
			  />
			</Form.Group>
		  

		  {/*Conditionally render submit button based on isActive state*/}
		  {isActive ?
		  		<Button variant="primary" type="submit" id="submitBtn"> Submit</Button>
		  		

		  	:
		  		<Button variant="danger" type="submit" id="submitBtn" disabled>Submit</Button>
		  }

		</Form>
	)


}